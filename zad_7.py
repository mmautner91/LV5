# -*- coding: utf-8 -*-
"""
Created on Sun Jan 10 21:54:11 2016

@author: marijan
"""


import numpy as np
#import matplotlib as plt
import sklearn.linear_model as lm
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.preprocessing import PolynomialFeatures
from sklearn import linear_model

br_tocnih_poly=br_net=tp=tn=fp=fn=x1=x2=0
def generate_data(n):
    #prva klasa
    n1 = n/2
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = n - n/2
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    data = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)
    data = data[indices,:]
    return data

#1 zad

np.random.seed(242)
xtrain=generate_data(500)
np.random.seed(242)
xtest=generate_data(500)

#zadatak 7
#ponovljen 3.zad i 5
#prosiravanje modela da sadrsi pun kvadrat, broj paramatara =5
poly = PolynomialFeatures(degree=2, include_bias = False)
xtrain_new = poly.fit_transform(xtrain[:,0:2])
#xtest_new = poly.fit_transform(xtrain[:,0:2])

#plt.scatter(xtrain_new[:,0],xtrain_new[:,1],c=xtrain_new[:,2])
LogRegModel3 = lm.LogisticRegression()
LogRegModel3.fit(xtrain_new,xtrain[:,2])

pred_poly=LogRegModel3.predict(xtrain_new[:,0:6]) #predvidanje rezultata
#print'print pred_poly', pred_poly
#a=np.shape(pred_poly)
#print 'a=np.shape(pred_poly)',a

theta03=LogRegModel3.intercept_
theta13=LogRegModel3.coef_[0,0]
theta23=LogRegModel3.coef_[0,1]
theta33=LogRegModel3.coef_[0,2]
theta43=LogRegModel3.coef_[0,3]
theta53=LogRegModel3.coef_[0,4]

print 'theta03',theta03
print 'theta13',theta13
print 'theta23',theta23
print 'theta33',theta33
print 'theta43',theta43
print 'theta53',theta53

'''
KAKO OVO PLOTAT UOPCE ????
'''

x13=(-theta03-theta13*(-7))/theta23  #lijeva koordinata

x23=(-theta03-theta13*7)/theta23  #desna koordinata
#plotanje pravca

plt.figure(4)
plt.title('graf podataka za ucenje sedmi zadatak, poly')
plt.scatter(xtrain_new[:,0],xtrain_new[:,1],c='rb')
#plt.plot(np.array([-6,6]),np.array([x13,x23]))
plt.show()


#print 'print np.shape(xtrain_new)',np.shape(xtrain_new)
#
#print xtrain_new

#TESTIRANJE TOCNOSI MODELA 


plt.figure (2)
plt.title('graf podataka za testiranjer_poly')
for i in range (len(xtest[:,2])): 
            if xtest[i:(i+1):,2]==pred_poly[i]:
                plt.scatter(xtest[i:(i+1):,0],xtest[i:(i+1):,1],c='g')
           
        #    if xtest[i:(i+1):,2]!=pred[i]:
            else:
                #boje netocno predvidjenih podataka sam prikazao plavom, puno bolja vidljivost na grafu
                   plt.scatter(xtest[i:(i+1):,0],xtest[i:(i+1):,1],color='b')
                  
#brojanje tp,tn,fn,fp
                  
for i in range (len(xtest[:,2])): 
    if ((xtest[i:(i+1):,2])==1 and (pred_poly[i])==1) :   #11
        tp+=1
    elif((xtest[i:(i+1):,2])==1 and (pred_poly[i])==0) :   #10
        fn+=1
    elif((xtest[i:(i+1):,2])==0 and (pred_poly[i])==0) :    #00
        tn+=1
    elif((xtest[i:(i+1):,2])==0 and (pred_poly[i])==1) :    #01
        fp+=1
    
print'print tp,tn,fp,fn  ', tp,tn,fp,fn    
                
                

#4.zad,ponovljen
#
#plt.figure(5)
#f, ax = plt.subplots(figsize=(8, 6))
#x_grid, y_grid = np.mgrid[min(xtrain_new[:,0])-0.5:max(xtrain_new[:,0])+0.5:.05,
#                          min(xtrain_new[:,1])-0.5:max(xtrain_new[:,1])+0.5:.05]
#grid = np.c_[x_grid.ravel(), y_grid.ravel()]
#probs = LogRegModel3.predict_proba(grid)[:, 1].reshape(x_grid.shape)
#
#cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)
#ax_c = f.colorbar(cont)
#ax_c.set_label("$P(y = 1|\mathbf{x})$")
#ax_c.set_ticks([0, .25, .5, .75, 1])
#ax.set_xlabel('$x_1$', alpha=0.9)
#ax.set_ylabel('$x_2$', alpha=0.9)
#ax.set_title('Izlaz logisticke regresije sedmi zadatak poli')
#plt.scatter(xtrain[:,0],xtrain_new[:,1],c='rb')
#plt.show()


#print 'np.shape(xtrain_new)',np.shape(xtrain_new)
#
#print 'xrain newq \n:  ',xtrain_new

def plot_confusion_matrix(c_matrix):
    norm_conf = []
    for i in c_matrix:
            a = 0
            tmp_arr = []
            a = sum(i, 0)
            for j in i:
                tmp_arr.append(float(j)/float(a))
            norm_conf.append(tmp_arr)
   
    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
  
    width = len(c_matrix)
    height = len(c_matrix[0])
    
    for x in xrange(width):
        for y in xrange(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x),
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)
    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()
matrica_zabune2=confusion_matrix(xtest[:,2],pred_poly)
plot_confusion_matrix(matrica_zabune2)

#izracun parametara po 5-10



accuracy_poly=accuracy_score(xtest[:,2],pred_poly)
missclacification_rate_poly=1-accuracy_poly
precision_score_poly=precision_score(xtest[:,2], pred_poly)  
recall_score_poly=recall_score(xtest[:,2],pred_poly) 

specificity_poly=float(tn/(tn+fp))

print 'accuracy_poly:',accuracy_poly
print 'missclacification_rate_poly:',missclacification_rate_poly
print 'precision_score_poly:',precision_score_poly
print 'recall_score_poly:',recall_score_poly
print 'specificity_poly :',specificity_poly





#Prosireni model je puno prezizniji 