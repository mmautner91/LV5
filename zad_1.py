# -*- coding: utf-8 -*-
"""
Created on Tue Jan  5 19:42:21 2016

@author: marijan
"""
import numpy as np
#import matplotlib as plt
import sklearn.linear_model as lm
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix

x1=x2=0
def generate_data(n):
    #prva klasa
    n1 = n/2
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = n - n/2
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    data = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)
    data = data[indices,:]
    return data

#1 zad

np.random.seed(242)
xtrain=generate_data(200)
np.random.seed(242)
xtest=generate_data(100)

#print xtrain[:,0]
#print xtest

#2.zadatak-prikaz podataka,drugacije boje
#zakomentirano jer odvoji plotove
#plt.title('graf podataka za ucenje')
#plt.scatter(xtrain[:,0],xtrain[:,1],c='rb')
#plt.show()


#3.zadatak izrada modela log.regeresije
#Napomena: granica odluke u ravnini x 1 − x 2 definirana je kao 
#krivulja: θ + θ1 x1 + θ1 x2 = 0
LogRegModel = lm.LogisticRegression()
LogRegModel.fit(xtrain[:,0:2],xtrain[:,2])

theta0=LogRegModel.intercept_
print theta0
theta1=LogRegModel.coef_[0,0]
print theta1
theta2=LogRegModel.coef_[0,1]
print theta2

#print 'ovo je aaaaaaaaaaaaa',a
#xsort1=np.sort(xtrain[:,1])
#x1=(-thet0-thet2*xsort1)/thet1
##print x1
##x1=x1[:,np.newaxis]
#print xsort
#xsort0=np.sort(xtrain[:,1])
#
#x2=(-thet0-thet1*xsort0)/thet2
#plt.plot(x1,x2)
#

x1=(-theta0-theta1*(-6))/theta2  #lijeva koordinata
print x2

x2=(-theta0-theta1*6)/theta2  #desna koordinata
print x2
#plotanje pravca

plt.figure(1)
plt.title('graf podataka za ucenje')
plt.scatter(xtrain[:,0],xtrain[:,1],c='rb')
plt.plot(np.array([-6,6]),np.array([x1,x2]))
plt.show()


print np.array([x1,x2])
#b=np.array([-6,6])
#print b
#print xtrain[:,0:2]
#print xtrain[:,2]
#print xtrain

#4.zad,bez linije


f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(xtrain[:,0])-0.5:max(xtrain[:,0])+0.5:.05,
                          min(xtrain[:,1])-0.5:max(xtrain[:,1])+0.5:.05]
grid = np.c_[x_grid.ravel(), y_grid.ravel()]
probs = LogRegModel.predict_proba(grid)[:, 1].reshape(x_grid.shape)

cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)
ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
ax.set_title('Izlaz logisticke regresije')
plt.scatter(xtrain[:,0],xtrain[:,1],c=xtrain[:,2])
plt.show()


#5.zadatak


#LogRegModel.predict(xtest[:,0:2],xtest[:,2])





#5.zadatak, izrada log modela
LogRegModel2=LogRegModel
#LogRegModel2 =  lm.LogisticRegression()
LogRegModel2.fit(xtest[:,0:2],xtest[:,2])
#predikcija izlazne velicine
pred=LogRegModel2.predict(xtest[:,0:2])
#racunanje theta
theta02=LogRegModel2.intercept_
print theta02
theta12=LogRegModel2.coef_[0,0]
print theta12
theta22=LogRegModel2.coef_[0,1]
print theta22
#racunanje koordinata pravca
x12=(-theta02-theta12*(-6))/theta22  #lijeva koordinata

x22=(-theta02-theta12*6)/theta22  #desna koordinata


#PETLJA USPOREDBE PREDVIDJENIH I STVARNIH VRIJEDNOSTI
plt.figure (2)
plt.title('graf podataka za testiranjer')
for i in range (len(xtest[:,2])): 
            if xtest[i:(i+1):,2]==pred[i]:
                plt.scatter(xtest[i:(i+1):,0],xtest[i:(i+1):,1],c='g')
        #    if xtest[i:(i+1):,2]!=pred[i]:
            else:
                #boje netocno predvidjenih podataka sam prikazao plavom, puno bolja vidljivost na grafu
                   plt.scatter(xtest[i:(i+1):,0],xtest[i:(i+1):,1],color='b')


plt.plot(np.array([-6,6]),np.array([x12,x22]))
plt.show()
#model ne predvidja dobro jedino "eksremnije" rezultate, odnosno one relativno udaljenije od centra


#a=len(xtest[:,2]) 
#b=xtest
#print 'duljina niiizaaaaaaaaaaaa',a
#plotanje pravca
#plt.figure(2)                # a second figure
#
#plt.scatter(xtest[:,0],xtest[:,1],c='rb')
#plt.plot(np.array([-6,6]),np.array([x12,x22]))






#print 'xtest \n', xtest
#print 'xtest[:,2]\n',xtest[1:3:,2]
#print 'xtest[:,1]',xtest[:,1]
#print 'xtest[:0,1]',xtest[:,0:3]
print 'pred [index]',pred[0]

#"print 'ono sto predvidja  predpredpred', pred





